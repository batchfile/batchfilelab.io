---
title: "Flutter Install Issue With Java Version"
date: 2020-08-10T18:30:08+10:00
draft: false
---

Encountered a problem when installing flutter and dart on macOS Catalina (version 10.15.5) when using the standard procedure https://flutter.dev/docs/get-started/install/macos

```shell
droo% flutter doctor
Doctor summary (to see all details, run flutter doctor -v):
[✓] Flutter (Channel stable, v1.17.5, on Mac OS X 10.15.5 19F101, locale en-AU)
[!] Android toolchain - develop for Android devices (Android SDK version 30.0.1)
    ✗ Android license status unknown.
      Try re-installing or updating your Android SDK Manager.
      See https://developer.android.com/studio/#downloads or visit visit
      https://flutter.dev/docs/get-started/install/macos#android-setup for detailed instructions.
[✓] Xcode - develop for iOS and macOS (Xcode 11.6)
[✓] Android Studio (version 4.0)
[✓] Connected device
```
Ok, there's a problem with the Android toolchain.. The "Android license status unknown" message may be a little misleading in this context. Before you take flutter doctor's advice to re-install or update the Android SDK Manager, check the verbose diagnostics.

```shell
droo% flutter doctor --android-licenses -v

ERROR: JAVA_HOME is set to an invalid directory: /Some/Directory/Path

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation.
```
Clearly in my case `$JAVA_HOME` environment variable is not set correctly. If your `$JAVA_HOME` is set correctly, then skip forward to the next step, otherwise Check what Java versions are available and set `$JAVA_HOME`

```shell
droo% /usr/libexec/java_home -V
Matching Java Virtual Machines (1):
    11.0.4, x86_64:	"Java SE 11.0.4"	/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home
```
In my case, only Java 11 is installed, I'll try it. 

```shell
setenv JAVA_HOME /Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home
```

Ok `$JAVA_HOME` is set correctly, check the verbose diagnostics again

```shell
droo% flutter doctor --android-licenses -v
Exception in thread "main" java.lang.NoClassDefFoundError: javax/xml/bind/annotation/XmlSchema
	at com.android.repository.api.SchemaModule$SchemaModuleVersion.<init>(SchemaModule.java:156)
	at com.android.repository.api.SchemaModule.<init>(SchemaModule.java:75)
	at com.android.sdklib.repository.AndroidSdkHandler.<clinit>(AndroidSdkHandler.java:81)
	at com.android.sdklib.tool.sdkmanager.SdkManagerCli.main(SdkManagerCli.java:73)
	at com.android.sdklib.tool.sdkmanager.SdkManagerCli.main(SdkManagerCli.java:48)
Caused by: java.lang.ClassNotFoundException: javax.xml.bind.annotation.XmlSchema
	at java.base/jdk.internal.loader.BuiltinClassLoader.loadClass(BuiltinClassLoader.java:583)
	at java.base/jdk.internal.loader.ClassLoaders$AppClassLoader.loadClass(ClassLoaders.java:178)
	at java.base/java.lang.ClassLoader.loadClass(ClassLoader.java:521)
	... 5 more
```

Ok there are still problems, in this case `java.lang.NoClassDefFoundError: javax/xml/bind/annotation/XmlSchema` is telling us in a round-about way that the Java version we are using does not support the Android SDK. This issue is a manifestation of [this StackOverflow question](https://stackoverflow.com/questions/46402772/failed-to-install-android-sdk-java-lang-noclassdeffounderror-javax-xml-bind-a). This Android SDK requires **Java 8** but I have some other version (in my case **Java 11**)

If you are encountering a different error or you are using Windows, there's a possible solution over here https://robbinespu.gitlab.io/blog/2020/03/03/flutter-issue-fixed-android-license-status-unknown-on-windows/

```shell
droo% /usr/libexec/java_home -V
Matching Java Virtual Machines (1):
    11.0.4, x86_64:	"Java SE 11.0.4"	/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home
```

Confirming **Java 8** is not hanging around, try installing it. There is no need to uninstall your existing version of Java, you can have multiple verisons of Java installed and use `$JAVA_HOME` to select one of them.

```shell
droo% brew cask install homebrew/cask-versions/adoptopenjdk8
```

Check the installation locations of the Java versions (there should be 2 versions installed now) and set `$JAVA_HOME` to the location for **Java 8**

```shell
droo% /usr/libexec/java_home -V
Matching Java Virtual Machines (2):
    11.0.4, x86_64:	"Java SE 11.0.4"	/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home
    1.8.0_262, x86_64:	"AdoptOpenJDK 8"	/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home

droo% setenv JAVA_HOME /Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home

droo% java -version
openjdk version "1.8.0_262"
OpenJDK Runtime Environment (AdoptOpenJDK)(build 1.8.0_262-b10)
OpenJDK 64-Bit Server VM (AdoptOpenJDK)(build 25.262-b10, mixed mode)
```

That should do it! So now when you run `flutter doctor --android-licenses -v` you *should* get a clear run through to the finish. The first time you run successfully through, you'll need to agree to a series of licenses. In the end `flutter doctor` should give you a clean bill of health.

```shell
droo% flutter doctor
Doctor summary (to see all details, run flutter doctor -v):
[✓] Flutter (Channel stable, 1.20.1, on Mac OS X 10.15.5 19F101, locale en-AU)
 
[✓] Android toolchain - develop for Android devices (Android SDK version 30.0.1)
[✓] Xcode - develop for iOS and macOS (Xcode 11.6)
[✓] Android Studio (version 4.0)
[✓] VS Code (version 1.47.3)
[✓] Connected device
```

Finished shaving that yak, back to coding.

